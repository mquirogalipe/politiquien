
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" type="image/png" href="../../img/icoUnsa.png"/>
  <title>SEGUNDA VUELTA</title>

  <!-- ESTILOS -->
  <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- SCRIPTS -->
  <cript src="../js/jquery-3.3.1.min.js"></script>

    <link href="../css/small-business.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.7.5/css/bootstrap-select.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.7.5/js/bootstrap-select.min.js"></script>
    <script src="../js/temas.js"></script>
  </head>

  <body onload="init();">
    <!-- Header -->
    <div id="page-wrapper">
        <div class="col-md-12 col-md-offset-2" class="container-fluid">
          <!-- PAGUE CONTENT -->

          <div id="temasL" name="temasL" ></div>
        </div>

    </div>
  </body>

