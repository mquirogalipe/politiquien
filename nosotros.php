<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="description" content="">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Politiquien</title>


  <link rel="icon" href="img/HK.ico">

  <link rel="stylesheet" href="style.css">
</head>
<body>

   <header class="header-area">


    <?php
    include 'menu2.php';
    ?>

</header>

<section class="about-area w-100 text-center" >
    <div class="container col-12 col-md-10 col-lg-8 ">

        <div class="row text-left">
            <div class="col-12 mt-5">
                <h2>¿Quiénes somos?</h2>
            </div>
        </div>

        <div class="row text-left">

            <div class="col-12 col-lg-6 my-2">
                <img class="w-100" src="img/team-politiquien.jpeg">
            </div>
            <div class="col-12 col-lg-6 my-2">
                <p align="justify">La comunidad Hacks Hackers en Arequipa se fundó el 23 de enero de 2018, en presencia de miembros de la comunidad Hacks Hackers Lima y el acompañamiento virtual de Mariano Blejman, fundador de Hacks Hackers Buenos Aires. Actualmente somos 70 miembros que nos juntamos alrededor del grupo de meetup <a href="https://www.meetup.com/es-ES/Hacks-Hackers-Arequipa/?_cookie-check=1DrbEMqW1Gs51xYJ" target="_blank"><font  size="3.5px" color="blue">HacksHackersArequipa</font></a>. Somos un grupo de periodistas interesados en la innovación del oficio, junto a desarrolladores o programadores, que desean poner sus conocimientos al servicio de la información. Nos motivó la inminencia de las elecciones regionales y municipales del 2018, en las que la gente necesita información confiable para elegir bien.</p>
            </div>
        </div>

        <div class="row text-left">
            <div class="col-12 mt-5">
                <h2>¿Qué es la comunidad de Hacks Hackers?</h2>
            </div>
        </div>

        <div class="row text-left">


            <div class="col-12 col-lg-12 my-2">

                <p align="justify">El mundo de los expertos informáticos y los periodistas se unen a medida que la información se vuelve digital y las empresas de Internet se convierten en imperios, reemplazando a los grandes medios de comunicación.</p>
                <p align="justify">Los periodistas se llaman a sí mismos &quot;hacks&quot;, alguien que puede generar palabras en cualquier situación. Los “hackers” usan el equivalente digital de cinta adhesiva para extraer el código. Por tanto, “Hacks-Hackers” intenta unir los dos mundos. </p>
                <p align="justify">Este grupo es para reunir a todas estas personas, quienes están trabajando para ayudar a otras personas a dar sentido a su mundo. Es para los hackers que exploran las tecnologías para filtrar y visualizar información. Y para los periodistas que usan la tecnología para buscar y contar historias. En la era de la sobrecarga de información, este trabajo se ha vuelto aún más crucial.</p>
            </div>
        </div>
    </div>
</section>
<!-- ##### About Area End ##### -->

<!-- ##### Team Area Start ##### -->
<section class="newspaper-team mb-30 w-100 text-cente">
    <div class="container col-12 col-md-10 col-lg-8">
        <div class="title"><h2 class="text-left my-5"> Miembros </h2>
        </div>


        <div class="row text-left">


            <div class="col-12 col-lg-6 my-2">
                <img class="w-100" src="imagenes/temas/miembros.jpeg">
            </div>

            <div class="col-12 col-lg-6 my-2">

                <p align="justify">En este desarrollo intervinieron los periodistas: Mabel Cáceres, Ibón Machaca, Maxi Quico, Claudia Beltrán, Fiorella Montaño, Mónica Cuti, Alex Cornejo y Ricardo Alarcón.En la segunda etapa, Eloy Deza, Marcos Veliz y Cristhian Cruz.</p>
                <p align="justify"> 
                Y los desarrolladores, Elvis Téllez, Juan Manuel Llano, Mauricio Quiroga Lipe y Herly Miranda.</p>
                <p align="justify">Con el apoyo de Jaku Emprende UNSA, Idea Internacional Perú, diario digital El Búho, la República, RPP, Radio Yaraví, Radio Star de Mollendo y Mollendinos.com. <br>
                Y el auspicio de Politiquem Brasil y Chicas Poderosas global.</p>
            </div>
        </div>

    </div>
</section>

<div class="contact-area w-100 text-center">
    <div class="container col-12 col-md-10 col-lg-8">
        <div class="row">
            <div class="col-12">
                <div class="contact-title text-left my-5">
                    <h2>Contáctenos</h2>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12 col-lg-8">
                <div class="contact-form-area">
                    <form action="#" method="post">
                        <div class="row">
                            <div class="col-12 col-lg-6">
                                <input type="text" class="form-control" id="name" placeholder="Nombre" required>
                            </div>
                            <div class="col-12 col-lg-6">
                                <input type="email" class="form-control" id="email" placeholder="correo" required="">
                            </div>
                            <div class="col-12">
                                <input type="text" class="form-control" id="subject" placeholder="Asunto">
                            </div>
                            <div class="col-12">
                                <textarea name="message" class="form-control" id="mensaje" cols="30" rows="10" placeholder="Mensaje"></textarea>
                            </div>
                            <div class="col-12 text-center">
                                <button class="btn newspaper-btn mt-30 w-100" type="submit">Enviar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <div class="col-12 col-lg-4">
                <div class="single-contact-information mb-10 text-left">
                    <h6>Teléfonos:</h6>
                    <p>+51 959 868 641</p>
                </div>
                <div class="single-contact-information mb-10 text-left">
                    <h6>Correo:</h6>
                    <p>politiquienaqp@gmail.com</p>
                </div>
            </div>
        </div>


    </div>
</div>
</div>

<div class="footer w-100 mt-5">
  <?php
  include 'FooterP.php';
  ?>
</div>


<script src="js/jquery/jquery-2.2.4.min.js"></script>
<!-- Popper js -->
<script src="js/bootstrap/popper.min.js"></script>
<!-- Bootstrap js -->
<script src="js/bootstrap/bootstrap.min.js"></script>
<!-- All Plugins js -->
<script src="js/plugins/plugins.js"></script>
<!-- Active js -->
<script src="js/active.js"></script>

</body>
</html>