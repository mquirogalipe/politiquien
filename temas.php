<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="description" content="">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Politiquien</title>
	<link rel="icon" href="img/HK.ico">
	<link rel="stylesheet" href="style.css">
	<script src="js/temas.js"></script>
</head>
<body onload="init();">

	<header class="header-area">
		<?php
		include 'menu2.php';
		?>
	</header>

	<!-- Content -->

	<div class="tema-area  w-100 text-center ">
		<div class="container col-12 col-md-10 col-lg-8">
			<div class="text-center pt-5 mb-4"> 
				<p class="text-titulo color-rojo text-bold">Temas </p>
			</div>
			<div class="contenido row" id="temas2" name="temas2" >	
			</div>	

			<div class="contenido row" id="infotemas" name="infotemas" >	



</div>
			</div>	
		</div>
	</div>
	<div class="footer w-100 mt-5">
		<?php
		include 'FooterP.php';
		?>
	</div>


	<script src="js/jquery/jquery-2.2.4.min.js"></script>
	<!-- Popper js -->
	<script src="js/bootstrap/popper.min.js"></script>
	<!-- Bootstrap js -->
	<script src="js/bootstrap/bootstrap.min.js"></script>
	<!-- All Plugins js -->
	<script src="js/plugins/plugins.js"></script>
	<!-- Active js -->
	<script src="js/active.js"></script>

</body>
</html>