
<!-- Footer -->
<style type="text/css">
img{
  width: 75%;
}
.tabla{
  display: table;
}
.row1{
  display: table-row;
}
.cell{
  display: table-cell;
}
</style>
<footer class="mt-5 py-4 bg-dark">
  <div class="container p-0">
    <div class="row">
      <div class="col-lg-6">
        <p class="text-white text-contenido ">Copyright &copy; PolitiQuién 2018 </p>
      </div>
      <div class="col-lg-6">
        <p class="text-contenido text-white text-bold">Contáctanos</p>
          <p class="text-white">Teléfono : +51 959 868 641</p>
          <p class="text-white">Correo : politiquienaqp@gmail.com</p>
      </div>

    </div>
  </div>
  <!-- /.container -->
</footer>