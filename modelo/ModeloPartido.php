<?php

class ModeloPartido extends Modelo {
    static $model ;
    public function __construct() {
        parent::__construct();
    }
    public static function getInstance(){
        if (self::$model== NULL){
            self::$model=new ModeloPartido();
        }
        return self::$model  ;
    }

    public function getLista() {

        $sql = "SELECT * FROM partidopolitico" ;
        $acentos = parent::getConn()->query("SET NAMES 'utf8'");
       
        $result = parent::getConn()->query($sql);
        $outp = array();
        $outp = $result->fetch_all(MYSQLI_ASSOC);
      //  echo json_encode($outp);
        return json_encode($outp);
    }

    public function getPartido($id) {

        $sql = "SELECT partidopolitico.partidopoliticonombre as nom FROM partidopolitico where `IdPartidoPolitico` = $id";
                $acentos = parent::getConn()->query("SET NAMES 'utf8'");

        $result = parent::getConn()->query($sql);
        $outp = array();
        $outp = $result->fetch_all(MYSQLI_ASSOC);
        return json_encode($outp);
    }

    public function getListaPostulantes($id) {

        $sql = "SELECT candidato.`idcandidato` as 'id',candidato.`idDistritoPostula` as distrito,"
                . "candidato.`idProvinciaPostula` as provincia,"
                . " candidato.`idDepartamentoPostula` as departamento,"
                . " distrito.distritonombre as distritonombre , "
                . "provincia.provincianombre as provincianombre , "
                . "departamento.departamentonombre as departamentonombre ,"
                . "CONCAT(candidato.`candidatonombre`, ' ' ,candidato.`candidatoapellido` ) "
                . "AS 'nombre',partidopolitico.`partidopoliticonombre` as 'partidonombre' ,"
                . " partidopolitico.`idpartidopolitico` as 'idpartido'FROM `candidato` "
                . "INNER JOIN partidopolitico  on candidato.`idagrupacion` =partidopolitico.`idpartidoPolitico` "
                . "LEFT JOIN distrito  on candidato.iddistritoPostula = distrito.iddistrito "
                . "LEFT JOIN provincia on candidato.idprovinciapostula = provincia.idProvincia "
                . "LEFT JOIN departamento on candidato.iddepartamentopostula = departamento.iddepartamento "
                . " WHERE candidato.`IdAgrupacion` =$id";
        //   $sql = "SELECT * FROM `departamento`";
        $acentos = parent::getConn()->query("SET NAMES 'utf8'");

        $result = parent::getConn()->query($sql);

        $outp = array();
        $i = 0;
        while ($fila = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
            $codigo = $fila ["id"];

            $Nombre = $fila ["nombre"];
            $distrito = $fila["distrito"];
            $provincia = $fila ["provincia"];
            $departamento = $fila ["departamento"];
            $cargo = "Alcalde Distrital";
            $lugar = $fila ["distritonombre"];
            if ($provincia != NULL ) {
                $cargo = "Alcande";
                $lugar= $fila["provincianombre"];
            }
            if ($departamento != NULL) {
                $cargo = "Gobernador  Regional ";
                $lugar = $fila ["departamentonombre"];
            }

            $partidoId = $fila ["idpartido"];
            $salida = array('id' => $codigo, 'lugar'=>$lugar,'nombre' => $Nombre, 'cargo' => $cargo, 'idpartido' => $partidoId);
            $outp[$Nombre] = $salida;
        }
        return json_encode($outp, JSON_UNESCAPED_UNICODE);
    }

}
