<?php
class ModeloTemas{

  private $db;

  public function __construct(){
    $this->db = Conectar::conexion();
  }
  public function getTemas(){
    $consulta = $this->db->query("SELECT tema.TemaNombre as tema, tema.TemaDescripcion as temaDes, temadetalle.IdCandidato1 as candidato1, temadetalle.TemaDetOpinion1 as opinion1, temadetalle.IdCandidato2 as candidato2, temadetalle.TemaDetOpinion2 as opinion2  FROM tema INNER JOIN temadetalle on temadetalle.IdTema=tema.TemaId WHERE temadetalle.TemaEstado=1");
    return $this->convertFormatJson($consulta);
  }
public function getCandidatoA(){
    $consulta = $this->db->query("SELECT candidato.IdCandidato as idCandidato, concat(candidato.CandidatoNombre,', ',candidato.CandidatoApellido) as nombre, partidopolitico.PartidoPoliticoNombre as partido FROM candidato INNER JOIN partidopolitico on candidato.IdAgrupacion=partidopolitico.IdPartidoPolitico WHERE candidato.IdCandidato=10");
    return $this->convertFormatJson($consulta);
  }
public function getCandidatoB(){
    $consulta = $this->db->query("SELECT candidato.IdCandidato as idCandidato, concat(candidato.CandidatoNombre,', ',candidato.CandidatoApellido) as nombre, partidopolitico.PartidoPoliticoNombre as partido FROM candidato INNER JOIN partidopolitico on candidato.IdAgrupacion=partidopolitico.IdPartidoPolitico WHERE candidato.IdCandidato=15");
    return $this->convertFormatJson($consulta);
  }
  function convertFormatJson($consulta){
    $datos = array();
    $posicion=0;
    while($fila = $consulta->fetch_assoc()) {
      $datos[$posicion] = $fila;
      $posicion++;
    }
    return $datos;
  }
}
?>
