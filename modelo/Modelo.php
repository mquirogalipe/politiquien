<?php

include_once './conexion/conexion.php';


if (class_exists('Modelo') != true) {

    class Modelo {

        private $conn;

        public function __construct() {
            $c = new Conexion();
            $this->conn = $c->getConexion();
        }

        public function getConn() {
            return $this->conn;
        }

        // retorna arreglo,para usar en un ajax json_encode a esta funcion
        public function getListaPorCampo($tabla, $campo, $valorCampo) {
            $sql = "SELECT * FROM `" . $tabla . "` WHERE `" . $campo . "`=" . $valorCampo;
            //echo $sql;
            
            $result = $this->conn->query($sql);
            $outp = array();
            $outp = $result->fetch_all(MYSQLI_ASSOC);
            return ($outp);
        }
      public  function titleCase($string) 
{
	
	  $string =ucwords(strtolower($string));

    foreach (array('-', '\'') as $delimiter) {
      if (strpos($string, $delimiter)!==false) {
        $string =implode($delimiter, array_map('ucfirst', explode($delimiter, $string)));
      }
    }
    return $string;
}

    }

}