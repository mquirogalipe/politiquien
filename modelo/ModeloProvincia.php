<?php

class ModeloProvincia extends Modelo  {

    public function __construct() {
        parent::__construct();
    }

    public function getLista() {

        $sql = "SELECT * FROM departamento where `departamentoEstado`=1";
        $result = parent::getConn()->query($sql);
        $outp = array();
        $outp = $result->fetch_all(MYSQLI_ASSOC);
       // echo json_encode($outp);
        return json_encode($outp);
    }
    
    public function getListaDistrito($id) {

        $sql = "SELECT distrito.iddistrito as id , distrito.distritonombre FROM distrito where `IdProvincia` = $id  and distrito.distritoestado = 1";
             $acentos = parent::getConn()->query("SET NAMES 'utf8'");

        $result = parent::getConn()->query($sql);
        $outp = array();
        $outp = $result->fetch_all(MYSQLI_ASSOC);
      //  echo json_encode($outp);
        return json_encode($outp);
    }
    
public function  getListaPostulantes($id){
       $sql = "SELECT candidato.`idcandidato` as 'id',provincia.provincianombre as provincia,CONCAT(candidato.`candidatonombre`, ' ' ,candidato.`candidatoapellido` ) AS 'nombre',partidopolitico.`partidopoliticonombre` as 'partidonombre' , partidopolitico.`idpartidopolitico` as 'idpartido'FROM `candidato` INNER JOIN partidopolitico  on candidato.`idagrupacion` =partidopolitico.`idpartidoPolitico` INNER JOIN provincia on candidato.idprovinciapostula=provincia.idprovincia WHERE candidato.`IdProvinciaPostula` =$id";
    //   $sql = "SELECT * FROM `departamento`";
       $acentos = parent::getConn()->query("SET NAMES 'utf8'");
// echo $sql ;
       $result = parent::getConn()->query($sql);
        
          $outp = array();
        $i=0 ;
        while ($fila = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
           $codigo = $fila ["id"];
        $provincia = $fila ["provincia"];
            $Nombre = $fila ["nombre"];
            $partido = $fila ["partidonombre"];
            $partidoId =$fila ["idpartido"];
             $salida = array('id'=>$codigo,'provincia'=>$provincia, 'nombre'=>$Nombre,'partidonombre'=>$partido,'idpartido'=>$partidoId  );
             $outp[$Nombre]=$salida;
              
               }
        return json_encode($outp, JSON_UNESCAPED_UNICODE );
    
    
}

public function  getListaPostulantesTodos(){
       $sql = "SELECT candidato.`idcandidato` as 'id',provincia.provincianombre as provincia,CONCAT(candidato.`candidatonombre`, ' ' ,candidato.`candidatoapellido` ) AS 'nombre',partidopolitico.`partidopoliticonombre` as 'partidonombre' , partidopolitico.`idpartidopolitico` as 'idpartido'FROM `candidato` INNER JOIN partidopolitico  on candidato.`idagrupacion` =partidopolitico.`idpartidoPolitico` INNER JOIN provincia on candidato.idprovinciapostula=provincia.idprovincia WHERE candidato.`IdProvinciaPostula` IS NOT NULL";
    //   $sql = "SELECT * FROM `departamento`";
       $acentos = parent::getConn()->query("SET NAMES 'utf8'");

       $result = parent::getConn()->query($sql);
        
          $outp = array();
        $i=0 ;
        while ($fila = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
           $codigo = $fila ["id"];
        $provincia = $fila ["provincia"];
            $Nombre = $fila ["nombre"];
            $partido = $fila ["partidonombre"];
            $partidoId =$fila ["idpartido"];
             $salida = array('id'=>$codigo,'provincia'=>$provincia, 'nombre'=>$Nombre,'partidonombre'=>$partido,'idpartido'=>$partidoId  );
             $outp[$Nombre]=$salida;
              
               }
        return json_encode($outp, JSON_UNESCAPED_UNICODE );
    
    
}
}
