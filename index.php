<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="description" content="">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Politiquien</title>
	<link rel="icon" href="img/HK.ico">
	<link rel="stylesheet" href="style.css">
	<script src="js/temas.js"></script>
	<script src = "// platform-api.sharethis.com/js/sharethis.js#property=5bff6a35ea80c50011bc5c1f&product=inline-share-buttons"> </script>

</head>

<body onload="init();">
	<header class="header-area">

		<?php
		include 'menu2.php';
		?>

	</header>


<div class = "sharethis-inline-share-buttons"> </div>


	<div class="candidato-area container  ">
		<div class="text-center pt-5 pb-4"> 
			<p class="text-titulo color-rojo text-bold">Segunda Vuelta</p>
			<p class="text-titulo color-rojo">Candidatos Regionales</p>
		</div>

		<div class="fotos text-center">
			<div class="contenido">	
				<div class="col-5 col-lg-4  candidato ">
					<a data-toggle='modal' data-target='#candi-1' href="#"><img class="w-75 " src="imagenes/fotoscandidatos/10.jpg" ></a>
					<a data-toggle='modal' data-target='#candi-1' href="#"><p class="text-subtitulo color-negro text-fuerte text-truncate  mt-2"><span id="nomCanA"> </span> </p></a>
					<a data-toggle='modal' data-target='#candi-1' href="#"><p class=" color-gris  text-truncate"> <span id="nomCanParA"> </span> </p></a>

				</div>
				<div class="col-5 col-lg-4  candidato" >
					<a data-toggle='modal' data-target='#candi-2' href="#"><img class="w-75 " src="imagenes/fotoscandidatos/15.jpg" ></a>
					<a data-toggle='modal' data-target='#candi-2' href="#"><p class="text-subtitulo color-negro text-fuerte text-truncate mt-2"><span id="nomCanB"> </span></p></a>
					<a data-toggle='modal' data-target='#candi-2' href="#"><p class=" color-gris text-truncate"> <span id="nomCanParB"> </span></p></a>

				</div>
			</div>	
		</div>
	</div>

	<div class="tema w-100 text-center">
		<div class="container col-12 col-md-10 col-lg-8 ">
			<p class="text-titulo color-rojo my-3"> Temas</p>
			<div id="temas" name="temas" ></div>
			<div class="contenido row" id="infotemas" name="infotemas" >
			</div>

			<div class="modal fade text-left" id="candi-1">
				<div class="modal-dialog modal-lg">
					<div class="container modal-content">
						<div class="modal-header">
							<h4 class="modal-title">Ísmodes Talavera Javier Enrique </h4>
							<button type="button" class="close" data-dismiss="modal">&times;</button>
						</div>
						<div class="row">
							<div class="col-12  col-lg-6">
								<img class="w-100" src="imagenes/fotoscandidatos/ISMODES.jpg">
								
								<br><br><h3>
								Ísmodes Talavera </h3>
								<h3>Javier Enrique</h3>
							
								<p><strong> Partido:</strong>
								Arequipa Transformación</p>
								<p><strong> DNI:</strong>
								29563543</p>
								<p><strong> Edad: </strong>
								47</p>


							</div>
							<div class="col-12 col-lg-6">
								<h3 class="color-rojo">Formacion profesional</h3>
								<p> -Estudios Posgrado - Maestría en Derecho Empresarial</p>
								<h3 class="color-rojo">Experiencia laboral</h3>
								<p> -Estudio Jurídico Muñiz, Ramírez, Perez Taimán y Luna Víctoria Abogados 2002-2016</p>
								<h3 class="color-rojo">Antecedentes Políticos</h3>
								<p> -Numero de veces que postula 3
									<br>-Militante en Frente Ciudad Futura 2006-2007
									<br>-Fue gerente de Comercio Exterior y Turismo en el 2007 y Jefe del Organismo de Promoción de la Inversión Privada del GRA en el mismo año. En 2014 fue candidato al GRA invitado por Arequipa Renace</p>
									<h3 class="color-rojo">Información económica</h3>
									<p> <strong>Bienes :</strong> 
										<br> -Tres inmuebles y un terreno , Una camioneta<br><br>
										<strong>Valor de Bienes :</strong>
										<br> S/ 2,000,314.00<br><br>
										<strong> Ingresos :</strong>
										<br>S/ 244,800.00<br><br>
										<strong>Empresas :</strong>
										<br>-Gerente general Ísmodes Abogados SAC
										<br>-Gerente general y accionista Escuela de Post Grado San Francisco Xavier. Sus acciones en esta institución están valuadas en S/ 350 mil</p>
									<h3 class="color-rojo">Antecedentes Judiciales</h3>
									<p><strong> Denuncias en Fiscalia :</strong>
										<br>Lista de expedientes Corte Superior de Justicia de Arequipa aparece como: Demandado 2002 Por definir; Demandado 2001 Elevado a sala; Demandado 2011 En Ejecución * Información Poder Judicial</p>
									
								</div>

								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar </button>
								</div>
							</div>
						</div>		
					</div>

					<div class="modal fade text-left" id="candi-2">
				<div class="modal-dialog modal-lg">
					<div class="container modal-content">
						<div class="modal-header">
							<h4 class="modal-title">Cáceres Llica Elmer</h4>
							<button type="button" class="close" data-dismiss="modal">&times;</button>
						</div>
						<div class="row">
							<div class="col-12  col-lg-6">
								<img class="w-100" src="imagenes/fotoscandidatos/CACERES.jpg">
								
								<br><br><h3>
								Cáceres Llica</h3>
								<h3>Elmer</h3>
								<p><strong> Partido:</strong>
								Unidos por el Gran Cambio</p>
								<p><strong> DNI:</strong>
								30642473</p>
								<p><strong> Edad: </strong>
								45</p>


							</div>
							<div class="col-12 col-lg-6">
								<h3 class="color-rojo">Formacion profesional</h3>
								<p> -Estudios Posgrado - Gestión Social</p>
								<h3 class="color-rojo">Experiencia laboral</h3>
								<p> -Alcalde de la Provincia de Caylloma 2003-2006 y 2011-2014</p>
								<h3 class="color-rojo">Antecedentes Políticos</h3>
								<p>-Numero de veces que postula 3
									<br>-Militante en Acción Popular 2004-2009
									<br>-Ha sido elegido dos veces como alcalde provincial de Caylloma, en 2002 por el movimiento Desarrolla Caylloma y en 2010 por Arequipa Avancemos, pero en las dos ocasiones renunció antes de que concluya su periodo para postular al gobierno regional. Postuló al GRA en 2006 por Arequipa Avancemos y en 2014 por Vamos Perú, sin éxito</p>
									<h3 class="color-rojo">Información económica</h3>
									<p> <strong>Bienes :</strong> 
										<br>1 departamento<br><br>
										<strong>Valor de Bienes :</strong>
										<br> No declara<br><br>
										<strong> Ingresos :</strong>
										<br>S/ 45,000.00<br><br>
										<strong>Empresas :</strong>
										<br>No declara</p>
									<h3 class="color-rojo">Antecedentes Judiciales</h3>

									<p><strong> Procesos :</strong>
										<br>-Entorpecimiento al funcionamiento del Medio se Transporte/Falsa Declaración en Proceso Con Sentencia Firme</p>
									<p><strong> Denuncias en Fiscalia :</strong>
										<br>-Peculado, Falsificación, Abuso de Autoridad, Violación Sexual Denunciado al PJ<br>Lista de expedientes Corte Superior de Justicia de Arequipa aparece como: Imputado 2005 En ejecución; Imputado 2008 Por definir y Denuncia por calificar; Imputado 2010 2013 y 2017 Resuelto; Imputado 2013 2014 y 2013 2014 y 2018 En trámite y Sentenciado ; * Información Poder Judicial</p>
									
								</div>

								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar </button>
								</div>
							</div>
						</div>		
					</div>





				</div>
			</div>

			<div class="footer w-100 mt-5">
				<?php
				include 'FooterP.php';
				?>
			</div>



			<script src="js/jquery/jquery-2.2.4.min.js"></script>
			<!-- Popper js -->
			<script src="js/bootstrap/popper.min.js"></script>
			<!-- Bootstrap js -->
			<script src="js/bootstrap/bootstrap.min.js"></script>
			<!-- All Plugins js -->
			<script src="js/plugins/plugins.js"></script>
			<!-- Active js -->
			<script src="js/active.js"></script>

		</body>
		</html>