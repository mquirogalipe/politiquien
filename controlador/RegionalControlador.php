<?php
require __DIR__.'/../modelo/Modelo.php';
require __DIR__.'/../modelo/ModeloRegion.php';
class Controller {
 public $model;

    public function __construct() {
        $this->model = new ModeloRegion();
    }

    public function invoke() {
            $postulantes = $this->model->getListaPostulantes();
       include './vista/RegionalVista.php';
     
            }
    }

$controller = new Controller();
$controller->invoke();
  ?>


