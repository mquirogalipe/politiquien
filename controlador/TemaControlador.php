<?php
//Archivo de configuracion
//require_once("../../conexion/db.php");
//require_once("../util/util.php");

//Llamada al modelo
require_once("../conexion/db.php");
require_once("../modelo/ModeloTemas.php");


$candidato = new ModeloTemas();

$accion = isset($_GET['accion']) ? $_GET['accion'] : "";


if($accion=='leerTemas'){
  $temas=$candidato->getTemas();
  $json = json_encode($temas);
  echo $json;
}else if($accion=="leerCandidatoA"){
  $candidatoA=$candidato->getCandidatoA();
  $json = json_encode($candidatoA);
  echo $json;
}else if($accion=="leerCandidatoB"){
  $candidatoB=$candidato->getCandidatoB();
  $json = json_encode($candidatoB);
  echo $json;
}else{
  echo jsonMensaje(true, "Error en URL");
}
?>
