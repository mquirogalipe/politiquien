<?php
require __DIR__.'/../modelo/Modelo.php';
require __DIR__.'/../modelo/ModeloDistrito.php';
class Controller {
 public $model;

    public function __construct() {
        $this->model = new ModeloDistrito();
    }

    public function invoke() {
       $idDistrito = $_POST['idDistrito'];
       $infoDistrito;
       $postulantes;
        if ($idDistrito== 0){
        $postulantes = $this->model->getListaPostulantesTodos();
          $infoDistrito = $this->model->getDistrito(1);
        
        }
        else {
           $infoDistrito = $this->model->getDistrito($idDistrito);
        $postulantes = $this->model->getListaPostulantes($idDistrito);
        
            
        }
        include './vista/DistritalVista.php';
     
            }
    }

$controller = new Controller();
$controller->invoke();
  ?>


