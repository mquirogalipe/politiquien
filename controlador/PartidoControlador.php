<?php
require __DIR__.'/../modelo/Modelo.php';
class Controller {
 public $model;

    public function __construct() {
        $this->model = new ModeloPartido();
    }

    public function invoke() {
       $ids = $_POST['id'];
  //     $ids= 5;
       $infoPartido = $this->model->getPartido($ids);
        $postulantes = $this->model->getListaPostulantes($ids);
       
            include './vista/PartidoVista.php';
     
            }
    }

$controller = new Controller();
$controller->invoke();
  ?>


