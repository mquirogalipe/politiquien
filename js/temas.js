var temas, candidatoA, candidatoB;
function cargarTemas(){

  $.when(
    $.ajax({url: "./controlador/TemaControlador.php?accion=leerTemas", success: function(result){
      temas = JSON.parse(result);
    }})
    ).done(function(ajaxAreas){
      llenarTemas();
    });
  }

  function cargarCandidatoA(){
    $.when(
      $.ajax({url: "./controlador/TemaControlador.php?accion=leerCandidatoA", success: function(result){
        candidatoA = JSON.parse(result);
      }})
      ).done(function(ajaxAreas){
      });
    }
    function cargarCandidatoB(){
      $.when(
        $.ajax({url: "./controlador/TemaControlador.php?accion=leerCandidatoB", success: function(result){
          candidatoB = JSON.parse(result);
        }})
        ).done(function(ajaxAreas){

        });
      }

      function llenarTemas(){
        var html="";
        var htmltemas="";
        var infotemas="";
        var canA=candidatoA[0].nombre;
        $("#nomCanA").text(canA);
        var canPartA=candidatoA[0].partido;
        $("#nomCanParA").text(canPartA);

        var canB=candidatoB[0].nombre;
        $("#nomCanB").text(canB);        
        var canPartB=candidatoB[0].partido;
        $("#nomCanParB").text(canPartB);

        var indiceTema=1;
        for(i in temas){
          //html+="<button class='btn col-12 col-lg-12 text-trucate my-1 px-4 border-20px d-flex justify-content-between align-items-center' type='button' data-toggle='collapse' data-target='#tema"+indiceTema+"' aria-expanded='false' aria-controls='tema1'> <img src='imagenes/temas/"+temas[i].tema+"_ico.png'>"; 
          html+="<button class='btn btn-tema btn-lg col-12 col-lg-12 text-trucate my-1' type='button' data-toggle='collapse' data-target='#tema"+indiceTema+"' aria-expanded='false' aria-controls='tema1'>";
          html+=temas[i].tema;
          html+="<i class='fa fa-angle-down ml-3'></i></button><div class='collapse  my-3' id='tema"+indiceTema+"'> <img class='mb-2 w-100' src='imagenes/temas/"+temas[i].tema+"_portada.jpg'> <div class='row'><div class='contenido-tema  text-left  col-6 '><div class='bg-white h-100 cuadro'><a class='color-rojo  ' href='index.php'><img class='mb-3' src='imagenes/fotoscandidatos/10.jpg' alt=''>";
          html+=canA;
          html+="</a><p>";
          html+=temas[i].opinion1;
          html+="</p><button  href='#' class='btn ml-2 float-right btn-danger' data-toggle='modal' data-target='#candi-1'>Candidato</button><a href='temas.php' class='btn float-right btn-warning text-white'>Tema</a></div>  </div><div class='contenido-tema  text-left  col-6 '><div class='bg-white cuadro h-100'>";
          //html+="</p><a href='#' class='btn btn-danger'>Candidato</a></a><a href='#' class='btn btn-warning' data-toggle='modal' data-target='#myModal"+indiceTema+"'>Tema</a></div>  </div><div class='contenido-tema h-100 text-left  col-6 '><div class='bg-white cuadro '>";
          
          html+="<a class='color-rojo ' href='index.php'><img class='mb-3' src='imagenes/fotoscandidatos/15.jpg' alt=''> ";
          html+=canB;
          html+="</a><p>";
          html+=temas[i].opinion2;
          html+="</p><button class='btn ml-2 float-right btn-danger' data-toggle='modal' data-target='#candi-2'>Candidato</button> <a href='temas.php' class='btn float-right btn-warning text-white' >Tema</a></div> </div></div> </div>  ";
          //html+="</p><a href='#' class='btn btn-danger'>Candidato</a> <a href='#' class='btn btn-warning' data-toggle='modal' data-target='#myModal"+indiceTema+"'>Tema</a></div> </div></div> </div>  ";

          htmltemas+="<div class='col-12 col-lg-4 col-sm-6 col-md-4 tema-espacio p-2 '><div class='tema m-1 h-100'><a href='' data-toggle='modal' data-target='#myModal"+indiceTema+"' ><img class='w-100 ' src='imagenes/temas/"+temas[i].tema+".jpg' ><p class='text-subtitulo color-negro text-fuerte  mt-2'>";
          htmltemas+=temas[i].tema;
          htmltemas+="</p><p class=' color-gris  text-truncate'> "; 
          htmltemas+="Click para mas información";
          htmltemas+="</p></a></div></div>";

          infotemas+="<div class='modal fade' id='myModal"+indiceTema+"'><div class='modal-dialog modal-lg'><div class='modal-content'><div class='modal-header'><h4 class='modal-title'>";
          infotemas+=temas[i].tema;
          infotemas+="</h4><button type='button' class='close' data-dismiss='modal'>&times;</button></div><div class='container my-2'><div class='text-left'><div class='col-12 col-lg-6 my-2 float-left'><img class='w-100' src='imagenes/temas/"+temas[i].tema+".jpg'></div><p>";
          infotemas+=temas[i].temaDes;
          infotemas+="</p></div></div><div class='modal-header'><h4 class='modal-title'>Opiniones</h4></div><div class='container'><div class='row my-2'><div class='contenido-tema  text-left  col-6 '><div class='bg-white h-100 cuadro'><a class='color-rojo' href='index.php'><img class='mb-3 img-pequeño' src='imagenes/fotoscandidatos/10.jpg' alt=''>";
          infotemas+=canA;  
          infotemas+="</a><p>";
          infotemas+=temas[i].opinion1;
          infotemas+="</p></div></div><div class='contenido-tema h-100 text-left  col-6 '><div class='bg-white cuadro '><a class='color-rojo' href='index.php'><img class='mb-3 img-pequeño' src='imagenes/fotoscandidatos/15.jpg' alt=''> ";
          infotemas+=canB;
          infotemas+="</a><p>";
          infotemas+=temas[i].opinion2;
          infotemas+="</p></div></div></div></div></div><div class='modal-footer'><button type='button' class='btn btn-secondary' data-dismiss='modal'>Cerrar </button></div></div></div></div>";

          indiceTema++;
        }
        $('#temas').html(html);
        $('#temas2').html(htmltemas);
        $('#infotemas').html(infotemas);
      }

      function init(){
        cargarCandidatoA();
        cargarCandidatoB();
        cargarTemas();
        

      }